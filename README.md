Schulische Facharbeit zum Thema
## Entwicklung eines virtuellen Bonsai-Rechners in der Simulationssoftware Logisim

- Schule: Hohenstaufen-Gymnasium Kaiserslautern
- Verfasser: Fabian Leo Schier	
- Betreuungslehrer: Dr. Michael Savorić
- Unterrichtsfach: Informatik
- Kursbezeichnung: Inf LK 1
- Abgabetermin:	26.5.2023