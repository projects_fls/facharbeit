##  Facharbeit: Hilfsprogramm zum Assemblieren
##  Autor: Fabian Leo Schier
##  Datum: 16.05.2023


def binaer(dezimalzahl, stellen):   # Konvertierung in Binärzahl
    binaerzahl = ""
    for _ in range(stellen):
        binaerzahl = str(dezimalzahl % 2) + binaerzahl
        dezimalzahl = dezimalzahl // 2
    return binaerzahl


def trenne_eingabe(eingabe):        # Trennung der eingegebenen Zeichenkette
    liste = []
    anfang = 0
    for zeichen in range(len(eingabe)):
        if eingabe[zeichen] in [" ", ",", ";"]:
            liste.append(eingabe[anfang:zeichen])
            anfang = zeichen+1
        elif zeichen == len(eingabe)-1:
            liste.append(eingabe[anfang:zeichen+1])
            anfang = zeichen+1
    return liste


def assembliere(liste):             # Umwandlung in Maschinensprache
    try:
        befehl = liste[0]
        
        if befehl.isnumeric() == True:
            ausgabe = binaer(int(befehl), 16)
        elif befehl == "HLT" or befehl == "hlt":
            ausgabe = "0000" + "0"*12
        elif befehl == "MOV" or befehl == "mov":
            ausgabe = "0001" + "0"*4 + binaer(int(liste[2]), 4) + binaer(int(liste[1]), 4)
        elif befehl == "ADD" or befehl == "add":
            ausgabe = "0010" + "0"*4 + binaer(int(liste[2]), 4) + binaer(int(liste[1]), 4)
        elif befehl == "SUB" or befehl == "sub":
            ausgabe = "0011" + "0"*4 + binaer(int(liste[2]), 4) + binaer(int(liste[1]), 4)
        elif befehl == "DBL" or befehl == "dbl":
            ausgabe = "0100" + "0"*8 + binaer(int(liste[1]), 4)
        elif befehl == "HLV" or befehl == "hlv":
            ausgabe = "0101" + "0"*8 + binaer(int(liste[1]), 4)
        elif befehl == "JMP" or befehl == "jmp":
            ausgabe = "0110" + "0"*8 + binaer(int(liste[1]), 4)
        elif befehl == "TST" or befehl == "tst":
            ausgabe = "0111" + "0"*8 + binaer(int(liste[1]), 4)
        elif befehl == "DIS" or befehl == "dis":
            ausgabe = "1000" + "0"*8 + binaer(int(liste[1]), 4)
        else:
            1/0
            
        return ausgabe[0:4] + "." + ausgabe[4:8] + "." + ausgabe[8:12] + "." + ausgabe[12:16]
    except:
        return "Fehler"


def eingabefunktion(programmnr):    # Funktion zur Ein- und Ausgabe
    programm = []
    zeile = 0
    
    print("–––––––––––––––––––––––––––––––––")
    print("Programm", programmnr)
    print("Bitte geben Sie Ihr Programm ein:"); print()

    while zeile < 16:
        eingabe = input("  {0:2d} : ".format(zeile))
        if eingabe != "":
            ausgabe = assembliere(trenne_eingabe(eingabe))
            if ausgabe != "Fehler":
                programm.append([binaer(zeile, 4), ausgabe])
                zeile += 1
            else:
                print(); print("Eingabefehler in Zeile", zeile); print()
                zeile = 17
        else:
            zeile += 1

    if zeile <= 16:
        print(); print("Ihr Programm in Maschinensprache:"); print()
        for befehl in programm:
            print("{0:s} : {1:s}".format(befehl[0], befehl[1]))
        print()

    return programmnr + 1

        
programmnr = 1                      # Hauptprogramm
while True:
    programmnr = eingabefunktion(programmnr)